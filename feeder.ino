/* 
 Controlling a servo position using a potentiometer (variable resistor) 
 by Michal Rinott <http://people.interaction-ivrea.it/m.rinott> 

 modified on 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Knob
*/

#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int potpin = 0;  // analog pin used to connect the potentiometer
int val;    // variable to read the value from the analog pin
int serv=13;
int aPin = 2;  //                     A
int bPin = 3;  //             ________
int cPin = 4;  //           |                   |
int dPin = 5;  //       F  |                   |  B
int ePin = 6;  //           |         G       |
int fPin = 7;  //            |________|
int gPin = 8;  //           |                   |
int GND1 = 9;  //        |                   |
int GND2 = 10; //   E |                   |   C
int GND3 = 11; //       |________|
int GND4 = 12; //       
int num;       //         D
int dig1 = 0;
int dig2 = 0;
int dig3 = 0;
int dig4 = 0;
int DTime = 2;

void setup()
{
    pinMode(aPin, OUTPUT);
  pinMode(bPin, OUTPUT);
  pinMode(cPin, OUTPUT);
  pinMode(dPin, OUTPUT);
  pinMode(ePin, OUTPUT); 
  pinMode(fPin, OUTPUT);
  pinMode(gPin, OUTPUT);
  pinMode(GND1, OUTPUT);
  pinMode(GND2, OUTPUT);
  pinMode(GND3, OUTPUT);
  pinMode(GND4, OUTPUT);
}

void open(int finish) {
  for(int angle=0; angle < finish; angle ++) {
    myservo.write(angle);
    delay(10);
  }

}

void close(int start) {
  for(int angle=start; angle > 0; angle --) {
    myservo.write(angle);
    delay(10);
  }

}

void shake(int minimum, int maximum, int times) {
  for(int i=0; i< times; i ++) {
    myservo.write(minimum);
    delay(25);
    myservo.write(maximum);
    delay(25);
  }
}

void blink(int times, int port) {
  for(int t=0; t < times; t ++) {
     digitalWrite(port, HIGH);
     delay(200);
     digitalWrite(port, LOW);
     delay(200);
  }
  
}

void waitquater(int hoursLeft, int quatersLeft) {
  for(int m=0; m<15; m ++) {
    for(int i=0; i < 7300; i ++) {
    setHour(hoursLeft);
    setMinute(quatersLeft * 15 + 14 - m);
   // delay(2);
    }
  }

}

void wait(int hours) {
  for(int h=0; h < hours; h ++) {
    for(int quater=0; quater < 4; quater ++) {
      myservo.attach(serv);
      shake(0, 2, 3);
      myservo.write(0);
      delay(100);
      myservo.detach();
      waitquater(hours - h - 1, 3 - quater);
    }
  }
}

void setHour(int hour) {
  int d1 = hour / 10;
  int d2 = hour % 10;
  digitalWrite( GND2, LOW);   //digit 2
  pickNumber(d2);
  delay(DTime);
  digitalWrite( GND2, HIGH);  

  digitalWrite( GND1, LOW);   //digit 1
  pickNumber(d1);
  delay(DTime);
  digitalWrite( GND1, HIGH);
}

void setMinute(int m) {
int d1 = m / 10;
  int d2 = m % 10;
  digitalWrite( GND4, LOW);   //digit 2
  pickNumber(d2);
  delay(DTime);
  digitalWrite( GND4, HIGH);  

  digitalWrite( GND3, LOW);   //digit 1
  pickNumber(d1);
  delay(DTime);
  digitalWrite( GND3, HIGH);
  
}
void loop() 
{ 
                  // sets the servo position according to the scaled value 
  myservo.attach(serv);
  open(20);
  shake(18, 21, 3);
  close(20);
  shake(0, 2, 3);
  delay(100);
  myservo.detach();
  delay(100);
  wait(6);
  //waitquater(5, 2);
} 

void pickNumber(int x){
   switch(x){
     case 1: one(); break;
     case 2: two(); break;
     case 3: three(); break;
     case 4: four(); break;
     case 5: five(); break;
     case 6: six(); break;
     case 7: seven(); break;
     case 8: eight(); break;
     case 9: nine(); break;
     default: zero(); break;
   }
}

void clearLEDs()
{  
  digitalWrite(  2, LOW); // A
  digitalWrite(  3, LOW); // B
  digitalWrite(  4, LOW); // C
  digitalWrite(  5, LOW); // D
  digitalWrite(  6, LOW); // E
  digitalWrite(  7, LOW); // F
  digitalWrite(  8, LOW); // G
}

void one()
{
  digitalWrite( aPin, LOW);
  digitalWrite( bPin, HIGH);
  digitalWrite( cPin, HIGH);
  digitalWrite( dPin, LOW);
  digitalWrite( ePin, LOW);
  digitalWrite( fPin, LOW);
  digitalWrite( gPin, LOW);
}

void two()
{
  digitalWrite( aPin, HIGH);
  digitalWrite( bPin, HIGH);
  digitalWrite( cPin, LOW);
  digitalWrite( dPin, HIGH);
  digitalWrite( ePin, HIGH);
  digitalWrite( fPin, LOW);
  digitalWrite( gPin, HIGH);
}

void three()
{
  digitalWrite( aPin, HIGH);
  digitalWrite( bPin, HIGH);
  digitalWrite( cPin, HIGH);
  digitalWrite( dPin, HIGH);
  digitalWrite( ePin, LOW);
  digitalWrite( fPin, LOW);
  digitalWrite( gPin, HIGH);
}

void four()
{
  digitalWrite( aPin, LOW);
  digitalWrite( bPin, HIGH);
  digitalWrite( cPin, HIGH);
  digitalWrite( dPin, LOW);
  digitalWrite( ePin, LOW);
  digitalWrite( fPin, HIGH);
  digitalWrite( gPin, HIGH);
}

void five()
{
  digitalWrite( aPin, HIGH);
  digitalWrite( bPin, LOW);
  digitalWrite( cPin, HIGH);
  digitalWrite( dPin, HIGH);
  digitalWrite( ePin, LOW);
  digitalWrite( fPin, HIGH);
  digitalWrite( gPin, HIGH);
}

void six()
{
  digitalWrite( aPin, HIGH);
  digitalWrite( bPin, LOW);
  digitalWrite( cPin, HIGH);
  digitalWrite( dPin, HIGH);
  digitalWrite( ePin, HIGH);
  digitalWrite( fPin, HIGH);
  digitalWrite( gPin, HIGH);
}

void seven()
{
  digitalWrite( aPin, HIGH);
  digitalWrite( bPin, HIGH);
  digitalWrite( cPin, HIGH);
  digitalWrite( dPin, LOW);
  digitalWrite( ePin, LOW);
  digitalWrite( fPin, LOW);
  digitalWrite( gPin, LOW);
}

void eight()
{
  digitalWrite( aPin, HIGH);
  digitalWrite( bPin, HIGH);
  digitalWrite( cPin, HIGH);
  digitalWrite( dPin, HIGH);
  digitalWrite( ePin, HIGH);
  digitalWrite( fPin, HIGH);
  digitalWrite( gPin, HIGH);
}

void nine()
{
  digitalWrite( aPin, HIGH);
  digitalWrite( bPin, HIGH);
  digitalWrite( cPin, HIGH);
  digitalWrite( dPin, HIGH);
  digitalWrite( ePin, LOW);
  digitalWrite( fPin, HIGH);
  digitalWrite( gPin, HIGH);
}

void zero()
{
  digitalWrite( aPin, HIGH);
  digitalWrite( bPin, HIGH);
  digitalWrite( cPin, HIGH);
  digitalWrite( dPin, HIGH);
  digitalWrite( ePin, HIGH);
  digitalWrite( fPin, HIGH);
  digitalWrite( gPin, LOW);
} 

